Dwengo Helvetica's installation utilities & information
=======================================================

Notebook list
-------------

dwengo01 HP Compaq nx8220

dwengo02 HP Compaq nc6220

dwengo03 HP Compaq nc6220

Operative system
-----------------
(as of 26.07.2020)

dwengo01 Debian 9 LXCE (last update 26.07.2020)

dwengo02 Debian 9 XFCE (last update 26.07.2020)

dwengo03 Debian 9 XFCE (last update 26.07.2020)

Installation procedure
----------------------

## System
1. Update and install packages as described in file `added_packages.txt`.

1. All computers have an adminstration user `juanpi`. Add it to sudoers with the commands:

     `su`

     `adduser juanpi sudo`
     
     The changes are effective in the next login.
     

1. Generate ssh keys for juanpi:

    `ssh-keygen -t rsa -b 4096`
    
    `ssh-add ~/.ssh/id_rsa`
    

1. (deprecated) *Add keys to bitbucket account dwengo_helvetica.*

1. Add public key to *pub_keys* files (to share with the other machines).

1. Set remote login using ssh keys only.

1. Create a guest user *dwengo*: `adduser dwengo`.
   Add it to groups tty and dialout: `adduser dwengo tty && adduser dwengo dialout`.

1. Remove modemmanager `apt-get purge modemmanager*`

## Arduino IDE
1. Download latest arduino IDE from arduino.cc 

1. Extract on `home/juanpi/Public/` and rename forlder `arduino-<version>` to `arduinoIDE`.

1. Copy `Arduino IDE.desktop` to desktop folder of user `dwengo` (i.e. `/home/dwengo/Desktop`). 

1. Install dwenguino board manager using the url provided in `dwenguino_manager_url.txt` as user `dwengo` executing the desktop launcher.
   If it is not working verify the installation steps at the [dwengo.org site](https://dwengo.org/2019/03/10/getting-acquainted-with-dwenguino-installation-for-windows-mac-os-x-and-linux/), the address to the board installer changes from time to time

1. (deprecated) *Install ardublocks* https://dwengo.org/2019/07/27/graphical-programming-with-ardublock/.

1. Install Dwenguino Blockly https://dwengo.org/2019/07/17/1175/. Check issue https://github.com/dwengovzw/Blockly-for-Dwenguino/issues/10

### Update
To update the arduino IDE:

1. Download latest arduino IDE from arduino.cc 

1. Remove forlder `/home/juanpi/arduinoIDE`.

1. Remove folder `/home/dwengo/.arduino15` (here the configuration of the IDE are stored)

1. Follow installation steps described above

## Blockly Games
1. Run the script `get_blocklygames.sh` to download Blockly Games in 
several languages

1. Run the script `do_blockly_launchers.sh` to generate desktop launchers

1. Copy all `blockly-games-<lang>` folders to `/home/juanpi/Public/`.

1. Copy folder `icons` to `home/juanpi/Public/`

1. Copy `blockly-games-<lang>.desktop` to desktop folder of user `dwengo` (i.e. `/home/dwengo/Desktop`). 

COMMENTS
--------
* dwengo02: Sometimes the usb controller uhci_hcd dies while programming. `dmesg` shows 
        host controller process error, something bad happened!
        host controller halted, very bad!
        HC died; cleaning up
  To recover we need to:
  1. Kill the process with avrdude.
  2. `modprobe -r uhci_hcd`
  3. `modprobe -r cdc_acm`
  4. `modprobe uhci_hcd`
  5. `modprobe cdc_acm`

* As of 29.05.2018 GNU Octave 4.4.0 is not yet in debian stable packages. 
  We use [Flatpack](https://flatpak.org/setup/) to get this version.

TODO
----

* Write a script to automatize the installation procedure.

* How to install arduino IDE, Dwenguino board, and ardublocks system wide?

