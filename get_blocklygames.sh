#!/bin/bash

for lang in de en es
do
  OUTDIR=blockly-games-${lang}
  wget "https://github.com/google/blockly-games/raw/offline/generated/${OUTDIR}.zip"
  unzip "${OUTDIR}"
  mv "blockly-games" "${OUTDIR}"
  rm "${OUTDIR}.zip"
done
