#!/bin/bash

for lang in de es en 
do
  LAUNCHER=blockly-games-${lang}.desktop
  echo "[Desktop Entry]" > $LAUNCHER
  echo "Version=1.0" >> $LAUNCHER
  echo "Type=Link" >> $LAUNCHER
  echo "Name=${lang}-BlocklyGames" >> $LAUNCHER
  echo "Comment=Blockly Games" >> $LAUNCHER
  echo "Icon=/home/juanpi/Public/icons/title-beta.png" >> $LAUNCHER
  echo "URL=/home/juanpi/Public/blockly-games-${lang}/index.html" >> $LAUNCHER
  chmod +x $LAUNCHER
done
